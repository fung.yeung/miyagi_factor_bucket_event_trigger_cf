import google.auth
from google.auth.transport.requests import Request
from google.oauth2 import id_token
import requests


def get_oauth_token(service_url: str) -> str:
    """
    return one time OAuth token that is required to trigger Cloud Function with HTTP trigger or Cloud Run

    Returns:
        id_token (str): authentication token to be add to header to trigger service

        HTTP request header adds "Authorization: bearer <id_token>"

    Ref:
        https://cloud.google.com/functions/docs/securing/authenticating#generating_tokens_programmatically
    """
    auth_req = google.auth.transport.requests.Request()
    id_token = google.oauth2.id_token.fetch_id_token(auth_req, service_url)
    return id_token


if __name__ == "__main__":

    AIRFLOW_RUNNER_CF_URL = (
        "https://us-central1-cn-ops-spdigital.cloudfunctions.net/airflow-api-runner"
    )
    print(get_oauth_token(service_url=AIRFLOW_RUNNER_CF_URL))
