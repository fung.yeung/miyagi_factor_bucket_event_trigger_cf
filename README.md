Trigger Airflow Factor ingestion dag when a dummy file "factor_export_completed_*"
landed in bucket.

Flow
1. Files finalized in bucket will all trigger this function
2. This function will only send a requests to airflow-api-runner when the filename is "factor_export_completed_*"