from email import header
from util.helper import get_oauth_token

# from google.cloud.functions import Context
from fnmatch import fnmatch
import requests

AIRFLOW_RUNNER_CF_URL = (
    "https://us-central1-cn-ops-spdigital.cloudfunctions.net/airflow-api-runner"
)


def miyagi_factor_bucket_trigger(event=None, context=None):
    """this function is triggered by bucket gs://datadesk-factor-asia-ingestion,
    if the newly landed file is the sentinel file "factor_export_completed" that indicate all files landed in GCS. Then trigger airflow ingestion DAG via API using another CF

    """

    fn = event["name"]
    fn_to_match = "factor_export_completed_*"

    # no need to do anything if file name doesn't match
    if not fnmatch(fn, fn_to_match):
        print(f'"{fn}" does not match keyword, would not trigger airflow ingestion dag')
        return

    print(f'"{fn}" detected')
    print("triggering airlfow factor ingestion DAG")
    token = get_oauth_token(service_url=AIRFLOW_RUNNER_CF_URL)
    auth_header = {"Authorization": f"Bearer {token}"}
    payload = {
        "method": "POST",
        "endpoint": "dags/test_send_grid/dagRuns",
        "kwargs": {"conf": {"msg": "this is triggered from bucket!"}},
    }

    # print(f"token is {token}")
    # print(f"auth_header is {auth_header}")
    # print(f"payload is {payload}")

    # trigger API without waiting it to finish
    requests.post(
        url=AIRFLOW_RUNNER_CF_URL,
        headers=auth_header,
        json=payload,
    )



if __name__ == "__main__":
    miyagi_factor_bucket_trigger()
